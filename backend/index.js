var bodyParser = require("body-parser");
const http = require("http");
const express = require("express");
const cors = require("cors");
const app = express();
require("dotenv").config();

app.use(cors());
const db = require("./models");
db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Connected to the database!");
  })
  .catch((err) => {
    console.log("Cannot connect to the database!", err);
    process.exit();
  });
app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.get("/", (req, res) => {
  res.json({ message: "Welcome to application." });
});
require("./router/teams.router")(app);
require("./router/history.router")(app);
require("./router/employeesID.router")(app);
var server;
var hostname = "localhost";
var port = 5000;

server = http.createServer(app);

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

// const express = require("express");
// const app= express();
// const cors = require("cors");
// app.use(cors());
// app.use(express.urlencoded({ extended: true }));

// app.use(express.json());

// // const { MongoClient } = require("mongodb")

// // const  url = "mongodb://localhost:27017"
// // const database = "NdEvent"
// // const connectiontoDb = new MongoClient(url);

// const routerevent = require("./router/teams.router")

// app.use("/postteam", routerevent);

// //  async function getData(){
// //      let result = await connectiontoDb.connect();
// //      let db = result.db(database)
// //      let collections = db.collection("NdTeams")
// //     //  console.log(collections.find({}).toArray());
// //      let response = await collections.find({}).toArray()
// //      console.log(response,'gggg');
// //  }

// //  getData()

// const port = 5000;

// app.listen(port)
// console.log(port);
