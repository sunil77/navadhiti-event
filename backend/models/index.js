const dbConfig = require("../config/dbconfig.js");

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = dbConfig.url;

db.teams = require("./teammembers.js")(mongoose);
db.comments = require("./teamComments.js")(mongoose);
db.NVEmpId= require("./employeesids.js")(mongoose);
db.confirmation= require("./confirmation.js")(mongoose);
db.beforeconfirmation=require("./beforeconfirmation")(mongoose)
db.afterconfirmation=require("./afterconfirmation.js")(mongoose)
module.exports = db;
