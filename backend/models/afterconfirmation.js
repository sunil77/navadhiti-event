module.exports = (mongoose) => {
  const Afterconfirmation = mongoose.model(
    "afterconfirmation",
    mongoose.Schema({
      event_name: {
        type: String,
      },
      confirmedteam: {
        type: Array,
      },
      uuid: {
        type: String,
      },
      createdAt: {
        type: Date,
        default: Date.now,
      },
      updatedAt: {
        type: Date,
        default: Date.now,
      },
    })
  );
  return Afterconfirmation;
};
