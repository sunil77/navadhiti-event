module.exports = (mongoose) => {
  const Confirmation = mongoose.model(
    "confirmation",
    mongoose.Schema({
   
      event_name: {
        type: String,
      },
      uuid: {
        type: String,
      },
      url: {
        type: String,
      },
      createdAt: {
        type: Date,
        default: Date.now 
      },
      updatedAt:{
          type:Date,
          default: Date.now 
      }
    })
  );
  return Confirmation;
};
