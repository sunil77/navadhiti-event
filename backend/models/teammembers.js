module.exports = (mongoose) => {
  const Teams = mongoose.model(
    "teammembers",
    mongoose.Schema({
      event_name: {
        type: String,
      },
      event_status: {
        type: Number,
      },
      event_img: {
        type: String,
      },
      start_date: {
        type: String,
      },
      end_date: {
        type: String,
      },
      description: {
        type: String,
      },
      event_rules: {
        type: Array,
      },
      teams: {
        type: Array,
      },
      createdAt: {
        type: Date,
      },
    })
  );
  return Teams;
};
