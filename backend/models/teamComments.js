module.exports = (mongoose) => {
    const Comments = mongoose.model(
      "teamComments",
      mongoose.Schema({
        eventComments: {
          type: Array,
          
        },
        event:{
          type: mongoose.Schema.Types.ObjectId,
          ref: 'teammembers'
        },
        createdAt:{
          type:Date
        }
      })
    );
    return Comments;
  };