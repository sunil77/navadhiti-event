module.exports = (mongoose) => {
    const Beforeconfirmation = mongoose.model(
      "beforeconfirmation",
      mongoose.Schema({
        event_name: {
          type: String,
        },
        event_img: {
          type: String,
        },
        uuid: {
          type: String,
        },
        start_date: {
          type: String,
        },
        end_date: {
          type: String,
        },
        description: {
          type: String,
        },
        event_rules: {
          type: Array,
        },
        createdAt: {
          type: Date,
          default: Date.now 
        },
        updatedAt:{
          type:Date,
          default: Date.now 
        }
      })
    );
    return Beforeconfirmation;
  };
  