const express = require("express");
const router = express.Router();

    const employeeesDetails = require("../controller/employees.controller");

module.exports = (app) => {
    router.get("/getEmployeesDetails", employeeesDetails.getEmployeesDetails);

    app.use("/getemployees", router);
}