const express = require("express");
const router = express.Router();
const teamDetails = require("../controller/teams.controller");
const upload = require("../middleware/eventImgs.middleware");

module.exports = (app) => {
  router.post("/postteamstoDb", upload.single("files"), teamDetails.postTeams);
  router.get("/getteamsfromDb", teamDetails.getTeams);
  router.get("/getteamsbyid/:id", teamDetails.getTeamById);
  router.get("/getteammembersbyid/:event/:id", teamDetails.getTeamMembersById);
  router.get(
    "/getteammembersbyids/:event/:id",
    teamDetails.getTeamMembersByIds
  );
  router.get("/getteammemberbyids/:event/:id", teamDetails.getteammemberbyids);
  router.put("/editteam/:_id", teamDetails.editTeams);
  router.get("/getAllEventsForEmailID", teamDetails.getAllEventsForEmailID);
  router.get("/getAllEventsForEmailIDs", teamDetails.getAllEventsForEmailIDs);
  router.get("/getAllHistoryForEmailiD", teamDetails.getAllHistoryForEmailiD);
  router.get(
    "/getAllCurrentEventsForEmailiD",
    teamDetails.getAllCurrentEventsForEmailiD
  );
  router.post("/postTeamComments", teamDetails.postTeamComments);
  router.get("/getTeamComments", teamDetails.getTeamComments);
  router.get("/getTeamConfirmation", teamDetails.getTeamConfirmation);
  router.post("/postconfirmation", teamDetails.postconfirmation);
  router.post("/getconfirmationlink", teamDetails.getConfirmationLink);
  router.post(
    "/getConfirmation",
    upload.single("files"),
    teamDetails.getConfirmation
  );

  app.use("/postteam", router);
  app.use("/getteam", router);
  app.use("/confirmation", router);
  app.use("/getteammembers", router);
  app.use("/edit", router);
  app.use("/confirmation", router);
};
