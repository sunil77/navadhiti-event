const express = require("express");
const router = express.Router();

const historyDetails = require("../controller/history.controller");

module.exports = (app) => {
  router.get("/eventendedteam", historyDetails.getEvents);
  router.get("/getendteambyid/:id", historyDetails.getEndTeamById);
  router.post("/postteamratingtoDb/:event/:id", historyDetails.postTeamrating);
  router.post("/postTeamRating/:event/:id", historyDetails.postTeamRating);

  app.use("/getteams", router);
  app.use("/getendteam", router);
  app.use("/postteamrating", router);
};
