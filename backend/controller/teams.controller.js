const models = require("../models");
const TeamMembers = models.teams;
const TeamComments = models.comments;
const TeamConfirmation = models.confirmation;
const Beforeconfirmation = models.beforeconfirmation;
const Afterconfirmation = models.afterconfirmation;
const jwt = require("jsonwebtoken")
const nodemailer = require("nodemailer");
const { v4: uuidv4 } = require("uuid");
const getConfirmation = async (req, res) => {
  let a = JSON.stringify(req.body);
  let s = a.replace(/[\u0000-\u0019]+/g, "");
  let o = JSON.parse(s);
  // console.log(req.body,'00000000000000000');
  let teamsData = JSON.parse(o.members);
  let teamRulesData = JSON.parse(o.event_rules);
  const url = process.env.API_URL;
  const uuid = uuidv4();
  // const expiresIn = '5s';
  let token = await jwt.sign({body:req.body},process.env.SECRET_TOKEN,{ expiresIn: 60, audience: '12345677' })
  console.log(token,'tyytuyuyuyuyy');
  const teamConfirmation = await new TeamConfirmation({
    uuid: uuid,
    url: url,
    event_name: o.event_name,
    createdAt: new Date(),
    updatedAt: new Date(),
  });
  // console.log(teamConfirmation, "000000000");
  const beforeconfirmation = new Beforeconfirmation({
    uuid: uuid,
    event_name: o.event_name,
    event_img: req.file.filename,
    event_name: o.event_name,
    start_date: o.start_date,
    end_date: o.end_date,
    description: o.description,
    event_rules: teamRulesData,
    teams: teamsData,
    createdAt: new Date(),
    updatedAt: new Date(),
  });

  // console.log(beforeconfirmation, "beforeconfirmation");
  
  if (o.event_name != null || o.event_name != undefined) {
    const myTimeout= await setTimeout(function() {
      jwt.verify(token, process.env.SECRET_TOKEN, function(err, payload) {
          if(err) {
              console.error('err: ', err);
          } else {
            const transporter = nodemailer.createTransport({
              service: "gmail",
              auth: {
                user: "srkalgi567@gmail.com",
                pass: "a78fad245ed4",
              },
            });
            for (let index = 0; index < teamsData.length; index++) {
              // let teamID = teamsData[index].team_id;
              // console.log(teamsData[index].email, "jghgffgfgg");
              var message = {
                from: "srkalgi567@gmail.com",
                to: teamsData[index].email,
                subject: "Confirmation for" + " " + o.event_name + " " + "event",
                html:
                  "<p>you have been invited to new event </p><a href='http://localhost:8080/login?event_name=" +
                  o.event_name +
                  "+&token=" +
                  token +
                  "#'>click here</a>",
              };
              transporter.sendMail(message, function (err, info) {
                if (err) {
                  console.log(err, "err");
                } else {
                  console.log("email sent" + info.response);
                }
              });
            }
          }
      });
  }, 1000);
  }
  await teamConfirmation.save(TeamConfirmation);
  await beforeconfirmation
    .save(beforeconfirmation)
    .then((data) => {
      res.status(200).send(data);
    })
    .catch((err) => {
      res.status(500).send(err);
    });

  //   // for (let index = 0; index < teamsData.length; index++) {
  //   //   // console.log(teamsData[index].team_id,'sadsfdsfdsfs');
  //   //   // console.log(req.finalData.teams.length)
  //   //   // console.log(teamObject,'teamObjectsss');
  //   //   //  let teamID = o.teams[index].team_id;
  //   //   let teamID = teamsData[index].team_id;
  //     // console.log(teamID,'========');
  //     // console.log( req.finalData.teams[index],'teamIDddd')
  //     // //     // 'arun.acharya@navadhiti.com'
  //     var message = {
  //       from: "srkalgi567@gmail.com",
  //       to: ["sunil@navadhiti.com"],
  //       subject: "confirmation for"+o.event_name,
  //       html:
  //         "<p>you have been invited to new event </p><a href='http://localhost:8080/login?teamId=" +
  //         "+&event_name=" +
  //         o.event_name +
  //         "#'>click here</a>",
  //     };
  //     transporter.sendMail(message, function (err, info) {
  //       if (err) {
  //         console.log(err, "err");
  //       } else {
  //         console.log("email sent" + info.response);
  //       }
  //     });
  //   // }
  // }
  // }
};
const getConfirmationLink = async (req, res) => {
  console.log(req.body.confirmationLink);
  let findTeam = await TeamConfirmation.findOne({
    uuid: req.body.confirmationLink.uuid,
  });
  let time = findTeam.createdAt;
  let createdTime = new Date(time);
  if (findTeam.uuid == req.body.confirmationLink.uuid) {
    let created = createdTime;
    let updated = new Date(new Date().setHours(new Date().getHours() + 1));
    // console.log(updated);
    if (created <= updated) {
      // console.log("hshdsdsdshd");
      res.status(200).send({ message: "link not expired" });
    } else {
      res.status(200).send({ message: "link expired" });
    }
  }
};
const postconfirmation = async (req, res) => {
  console.log(req.bodyconfirmationMsg, "hhhhhhhhhhh");
  let obj = req.body.confirmationMsg;
  let checkEmail = req.body.confirmationMsg.email;
  let confirmedteam = [];
  let checkUuid = req.body.uuid;
  // console.log(checkUuid);
  let findUuid = await TeamConfirmation.findOne({
    uuid: checkUuid,
  });
  // console.log(findUuid.uuid, req.body.confirmationMsg.uuid, "sameeee");
  if (findUuid.uuid == checkUuid) {
    // console.log(checkUuid);
    // confirmedteam.push(req.body.confirmationMsg);
    // console.log(confirmedteam, "confirmedteam");
    // let findconfirm = await Afterconfirmation.findOne({
    //   uuid: checkUuid,
    // })
    // console.log(findconfirm,'llllllllllll');
    let findBeforeInsertingData = await Afterconfirmation.findOne({
      uuid: checkUuid,
    });
    // console.log(findBeforeInsertingData.confirmedteam, "yyyyyyyyyyyyyyyyyyy");
    let arr = [];

    //  console.log(foundEmail);
    if (
      findBeforeInsertingData == undefined ||
      findBeforeInsertingData == null
    ) {
      console.log("jjjjjjjjjjj");
      confirmedteam.push(req.body.confirmationMsg);
      const afterconfirmation = await new Afterconfirmation({
        uuid: checkUuid,
        confirmedteam: confirmedteam,
        createdAt: new Date(),
        updatedAt: new Date(),
      });
      // console.log(afterconfirmation, "erererer");
      afterconfirmation
        .save(afterconfirmation)
        .then((data) => {
          res.status(200).send(data);
        })
        .catch((err) => {
          res.status(200).send(err);
        });
    }
    arr = findBeforeInsertingData.confirmedteam;
    let foundEmail = arr.find((x) => x.email == checkEmail);
    if (findBeforeInsertingData.uuid == checkUuid && foundEmail != checkEmail) {
      // confirmedteam = [...confirmedteam, obj]
      // console.log(confirmedteam,'kkkkkkkkkkk');
      confirmedteam = [...confirmedteam, obj];
      // console.log(confirmedteam, "kkkkkkkkkkk");
      const afterconfirmation = await Afterconfirmation.updateOne(
        { _id: findBeforeInsertingData._id },
        {
          $addToSet: {
            confirmedteam: confirmedteam,
          },
        }
      );
      // console.log(afterconfirmation, "[[[[[[[[[[[");
      if (afterconfirmation.modifiedCount == 1) {
        res.status(200).send("success");
      } else {
        res.status(200).send("err");
      }
    } else {
      res.status(200).send("err");
    }
  } else {
    res.status(200).send({ message: "not confirmation" });
  }
};
const getTeamConfirmation = async (req, res) => {
  Afterconfirmation.find({})
    .then((data) => {
      console.log(data, "dataaaaaaaaaaaaa");
      return res.status(200).send(data);
    })
    .catch((err) => {
      console.log(err, "err");
    });
};
const postTeams = async (req, res) => {
  console.log(req.body, "llll");
  let a = JSON.stringify(req.body);
  let s = a.replace(/[\u0000-\u0019]+/g, "");
  var o = JSON.parse(s);
  let teamRulesData = JSON.parse(o.event_rules);
  console.log(o.event_rules);
  let teamsData = JSON.parse(o.teams);
  const Teams = await new TeamMembers({
    event_img: req.file.filename,
    event_name: o.event_name,
    event_status: o.event_status,
    start_date: o.start_date,
    end_date: o.end_date,
    description: o.description,
    event_rules: teamRulesData,
    teams: teamsData,
    createdAt: new Date(),
  });
  // // console.log(Teams,'000000000');
  if (o.event_status == 1) {
    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "srkalgi567@gmail.com",
        pass: "a78fad245ed4",
      },
    });
    for (let index = 0; index < teamsData.length; index++) {
      // console.log(teamsData[index].team_id,'sadsfdsfdsfs');
      // console.log(req.finalData.teams.length)
      // console.log(teamObject,'teamObjectsss');
      //  let teamID = o.teams[index].team_id;
      let teamID = teamsData[index].team_id;
      // console.log(teamID,'========');
      // console.log( req.finalData.teams[index],'teamIDddd')
      // //     // 'arun.acharya@navadhiti.com'
      var message = {
        from: "srkalgi567@gmail.com",
        to: ["sunil@navadhiti.com"],
        subject: "New event has been generated",
        html:
          "<p>you have been invited to new event </p><a href='http://localhost:8080/login?teamId=" +
          teamID +
          "+&event_name=" +
          o.event_name +
          "#'>click here</a>",
      };
      transporter.sendMail(message, function (err, info) {
        if (err) {
          console.log(err, "err");
        } else {
          console.log("email sent" + info.response);
        }
      });
    }
  }
  Teams.save(Teams)
    .then((data) => {
      res.status(200).send(data);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
};
//
const getTeams = async (req, res) => {
  TeamMembers.find({
    event_status: 1,
  })
    .sort({ createdAt: -1 })
    .then((data) => {
      const result = {
        status_msg: "Success",
        data: data,
      };
      return res.status(200).send(result);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving data ",
      });
    });
};
//
const getTeamById = async (req, res) => {
  await TeamMembers.findOne({
    _id: req.params.id,
  })
    .then((data) => {
      // console.log(data,'--------');
      const result = {
        status_msg: "Success",
        data: data,
      };
      return res.status(200).send(result);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving data ",
      });
    });
};
//
const getTeamMembersById = async (req, res) => {
  // console.log("gdsagd",req.params.event);
  // console.log("gdsagd",req.params.id);
  let team_id = req.params.id;
  // console.log("gdsagd",req);
  let findTeam = await TeamMembers.findOne({
    _id: req.params.event,
  });
  let team = findTeam;
  // console.log(team);
  let foundIndex = team.teams.find((x) => x.team_id == team_id);
  // console.log(foundIndex, "foundIndex")
  let result = {
    team,
    foundIndex,
  };
  return res.status(200).send(result);
};
//
const getteammemberbyids = async (req, res) => {
  // le.log("gdsagd",req.params.event);
  // console.log("gdsagd", req.params.id);
  // console.log("dddd", req.params.event);
  let team_id = req.params.event;
  // console.log("gdsagd",req);
  let findTeam = await TeamMembers.findOne({
    _id: req.params.id,
  });
  let team = findTeam;
  // console.log(team);
  let foundIndex = team.teams.find((x) => x.team_id == team_id);
  // console.log(foundIndex, "foundIndex")
  let result = {
    team,
    foundIndex,
  };
  return res.status(200).send(result);
};
//
const getTeamMembersByIds = async (req, res) => {
  // console.log("gdsagd",req.params.event);
  // console.log("gdsagd",req.params.id);
  let team_id = req.params.id;
  // console.log("gdsagd",req);
  let findTeam = await TeamMembers.findOne({
    _id: req.params.event,
  });
  let team = findTeam;
  // console.log(team);
  let foundIndex = team.teams.find((x) => x.team_id == team_id);
  // console.log(foundIndex, "foundIndex")
  let result = {
    team,
    foundIndex,
  };
  return res.status(200).send(result);
};
//
const editTeams = async (req, res) => {
  // console.log(req.params._id, "ppppp");

  // let result = req.params._id
  let updateTeam = await TeamMembers.updateOne(
    { _id: req.params._id },
    {
      $set: {
        event_status: 2,
      },
    }
  );
  // console.log(updateTeam);
  if (updateTeam.modifiedCount == 1) {
    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "srkalgi567@gmail.com",
        pass: "a78fad245ed4",
      },
    });
    // for (let index = 0; index < req.body.result_teams.teams.length; index++) {
    //   let teamID=req.body.result_teams.teams[index].team_id
    //   // 'arun.acharya@navadhiti.com'
    //    var message = {
    //     from: 'srkalgi567@gmail.com',
    //     to: ['sunil@navadhiti.com', ],
    //     subject: 'New Event as been Generated',
    //     html: "<p>you have been invited to new event </p><a href='http://localhost:8080/login?teamId="+teamID+"+&event_name="+req.body.result_teams.event_name+"#'>click here</a>",
    //   };
    //   transporter.sendMail(message, function (err, info) {
    //     if (err) {
    //       console.log(err, 'err');
    //     } else {
    //       console.log('email sent' + info.response);
    //     }
    //   });
    var message = {
      from: "srkalgi567@gmail.com",
      to: "sunil@navadhiti.com",
      subject: "Event Ended",

      text: "this is event end body",
    };
    transporter.sendMail(message, function (err, info) {
      if (err) {
        console.log(err, "err");
      } else {
        console.log("email sent" + info.response);
      }
    });
  }
};
//
const getAllEventsForEmailID = (req, res) => {
  // console.log(req.query.event_name,'lll');
  // console.log(req.query.teamID,'lll');
  // console.log(req.query,'lll');
  let resultJson = [];
  TeamMembers.find({
    event_status: 1,
    event_name: req.query.event_name,
  })
    .then((data) => {
      // console.log(data,'000');
      data.map((item) => {
        // console.log(item);
        item.teams.map((team) => {
          // console.log(team,'000');
          team.team_member.map((player) => {
            //  console.log(player,'---');
            if (player.email == req.query.emailId) {
              //  console.log(team.team_member)
              resultJson = {
                event: item,
                teams: team,
              };
            }
          });
        });
      });
      // console.log(resultJson,'000');

      // let resultJson = {
      //   event: data[0],
      //   my_team: item,
      // };
      return res.status(200).send(resultJson);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving data ",
      });
    });
};
//
const getAllHistoryForEmailiD = (req, res) => {
  // console.log("llll", req.query)
  let history = [];
  TeamMembers.find({
    event_status: 2,
  })
    .then((data) => {
      data.map((item) => {
        item.teams.map((team) => {
          team.team_member.map((player) => {
            if (player.email == req.query.email) {
              // console.log(team.team_member)
              history.push({
                event: item,
                teams: team,
              });
            }
          });
        });
      });
      // if(history && history.length > 0) {
      // let result={
      //   team: history,
      //   historyData:data
      // }
      // console.log(history);
      res.status(200).send({
        team: history,
      });
      // }
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving data ",
      });
    });
};
//
const getAllCurrentEventsForEmailiD = (req, res) => {
  //  console.log(req.query,'bodyyyyy');
  let currentEvents = [];
  TeamMembers.find({
    event_status: 1,
  })
    .then((data) => {
      //  console.log(data,'dataaa');
      data.map((item) => {
        item.teams.map((team) => {
          team.team_member.map((player) => {
            if (player.email == req.query.email) {
              // console.log(team.team_member)
              currentEvents.push({
                event: item,
                teams: team,
              });
            }
          });
        });
      });
      //  console.log(currentEvents,'currentEvents');
      res.status(200).send({
        team: currentEvents,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving data ",
      });
    });
};
//
const postTeamComments = (req, res) => {
  // console.log(req.body,'bodyggggg');
  // 6193569e5855f074efd5619e
  const Comments = new TeamComments({
    eventComments: req.body.teamComments,
  });
  // console.log(Comments,'9999');
  Comments.save(Comments)
    .then((data) => {
      // console.log(data,'000');
      res.status(200).send(data);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
};
//
const getTeamComments = (req, res) => {
  console.log(req.query, "7777777");
  // console.log(req.query.eventID,'7777777');

  let eventTeamComments = [];
  TeamComments.find({
    // event_id:req.query.eventID,
    // emailID:req.query.emailID
  })
    .then((response) => {
      // console.log(response,'dataaaaaa');
      response.map((item) => {
        // console.log(item.eventComments);
        item.eventComments.map((ele) => {
          // console.log(ele.event_id);

          if (
            ele.event_id == req.query.eventID &&
            ele.team_id == req.query.team_id &&
            ele.eventName == req.query.eventName
          ) {
            eventTeamComments.push(item.eventComments);

            // res.status(200).send({data:eventTeamComments});
          }
        });
      });
      // console.log(eventTeamComments);
      res.status(200).send(eventTeamComments);
    })
    .catch((err) => {
      console.log(err, "errr");
    });
};
//
const getAllEventsForEmailIDs = (req, res) => {
  // console.log(req.query,'bodyyyyy');
  let history = [];
  TeamMembers.find({
    event_status: 1,
  })
    .then((data) => {
      // console.log(data,']]]]]]]]]');
      data.map((item) => {
        //  console.log(item);
        item.teams.map((team) => {
          //  console.log(team);
          team.team_member.map((player) => {
            if (player.email == req.query.email) {
              // console.log(team.team_member)
              history.push({
                event: item,
                teams: team,
                event_img: item.event_img,
              });
            }
          });
        });
      });
      if (history && history.length > 0) {
        res.status(200).send({
          team: history,
        });
        // let result={
        //   team: history,
        //   historyData:data
      }
      // console.log(history);

      // }
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving data ",
      });
    });
};
//
module.exports = {
  postTeams,
  getTeams,
  getTeamById,
  getTeamMembersById,
  editTeams,
  getAllEventsForEmailID,
  getAllHistoryForEmailiD,
  getAllCurrentEventsForEmailiD,
  postTeamComments,
  getTeamComments,
  getTeamConfirmation,
  // getHistoryTeammembersById,
  getAllEventsForEmailIDs,
  getTeamMembersByIds,
  getteammemberbyids,
  getConfirmation,
  getConfirmationLink,
  postconfirmation,
};
