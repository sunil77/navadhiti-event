const models = require("../models");
const TeamMembers = models.teams;

const getEvents = async (req, res) => {
  TeamMembers.find({
    event_status: 2,
  })
    .then((data) => {
      const result = {
        status_msg: "Success",
        data: data,
      };
      return res.status(200).send(result);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving data ",
      });
    });
};
//
const getEndTeamById = async (req, res) => {
  // console.log(req.params.id,'uuuu');
  TeamMembers.findOne({
    _id: req.params.id
  })
    .then((data) => {
      const result = {
        status_msg: "Success",
        data: data,
      };
      return res.status(200).send(result);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving data ",
      });
    });
}
//hr app postrating
const postTeamrating = async (req, res) => {
  // console.log(req.body.ratingteam,'qqq');
  let team_id = req.params.id
  let findTeam = await TeamMembers.findOne({
    _id:req.params.event
  })
  // console.log(findTeam,'rrr');
  let foundIndex = findTeam.teams.findIndex(x => x.team_id == team_id);
  // console.log(foundIndex, "foundIndex")
  // findTeam.teams[foundIndex] = req.body.ratingTeam;
  findTeam.teams[foundIndex].team_rating = req.body.ratingteam.teamRating;
  // console.log(findTeam, "findTeam")
  let myquery = { _id: findTeam._id };

  TeamMembers.updateOne(myquery, findTeam, { upsert: true })
    .then((data) => {
      // console.log(data, "updated")
      res.status(200).send(data)
    })
    .catch((err)=>{
      console.log(err,'err');
    })
};
// app postrating
const postTeamRating =async(req,res)=>{
  // console.log(req.params,'params');
  // console.log(req.body,'body');
  let team_id = req.params.event
  let findTeam = await TeamMembers.findOne({
    _id:req.params.id
  })
  // console.log(findTeam,'...');
  let foundIndex = findTeam.teams.findIndex(x => x.team_id == team_id);
  // console.log(foundIndex,'kkk');
  findTeam.teams[foundIndex].captain_rating = req.body.ratingteam.teamRating;
  let myquery = { _id: findTeam._id };
  TeamMembers.updateOne(myquery, findTeam, { upsert: true })
  .then((data) => {
    // console.log(data, "updated")
    res.status(200).send(data)
  })
  .catch((err)=>{
    console.log(err,'err');
  })
}

module.exports = { getEvents,getEndTeamById, postTeamrating,postTeamRating };
