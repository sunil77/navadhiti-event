import Vue from "vue";
import Vuex from "vuex";
// import authModule from './modules/auth';
// import createPersistedState from 'vuex-persistedstate';

// Vue.use(Vuex);

// export default new Vuex.Store({
//   modules: {
//     auth: authModule,
//   },
//   plugins: [createPersistedState()],
// });
import * as fb from "../firebase";
import router from "../router/index";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userProfile: {},
    drawer: null,
    isRating:false
  },
  mutations: {
    setUserProfile(state, val) {
      state.userProfile = val;
    },
    SET_SIDEBAR_DRAWER(state, payload) {
      console.log(payload);
      state.drawer = payload;
    },
    setRating(state, val) {
      state.isRating = val;
    },
  },
  actions: {
    async login({ dispatch }, form) {
      // sign user in
      const { user } = await fb.auth.signInWithEmailAndPassword(
        form.email,
        form.password
      );
      // fetch user profile and set in state
      dispatch("fetchUserProfile", user);
    },
    async fetchUserProfile({ commit }, user) {
      // fetch user profile
      const userProfile = await fb.usersCollection.doc(user.uid).get();

      // set user profile in state
      commit("setUserProfile", userProfile.data());

      // change route to dashboard
      router.push("/events");
    },
  },
});
