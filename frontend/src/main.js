import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import router from "./router";
import vuetify from "./plugins/vuetify";
import CKEditor from '@ckeditor/ckeditor5-vue2';
// import firebase from "firebase";
import moment from "moment";
Vue.use( CKEditor );
Vue.config.productionTip = false;
Vue.filter("formatDate", function (value) {
  console.log(value);
  if (value) {
    return moment(String(value)).format("DD MMM yyyy");
  }
});
import GAuth from "vue-google-oauth2";
const gauthOption = {
  clientId:
    "504153648690-qmdksrgbict7fg0eruulii8154uqv8um.apps.googleusercontent.com",
  scope: "profile email",
  prompt: "consent",
  fetch_basic_profile: true,
};
Vue.use(GAuth, gauthOption);
new Vue({
  store,
  router,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
