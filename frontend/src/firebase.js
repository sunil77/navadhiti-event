import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

// firebase init - add your own config here
const firebaseConfig = {
  apiKey: "AIzaSyD3OpAHyxJHymhUXDH0AGCxdVrVabyTqkA",
  authDomain: "eventnd-ba103.firebaseapp.com",
  //   authDomain: 'eventnd-ba103',
  storageBucket: "eventnd-ba103.appspot.com",
  //   databaseURL: '',
  projectId: "eventnd-ba103",
  //   storageBucket: '',
  messagingSenderId: "681432952038",
  appId: "1:681432952038:web:ead040d38d649da9f29f27",
  measurementId: "G-0RZ9N3M1YB",
};
firebase.initializeApp(firebaseConfig);

// utils
const db = firebase.firestore();
const auth = firebase.auth();

// collection references
const usersCollection = db.collection("users");
const postsCollection = db.collection("posts");
const commentsCollection = db.collection("comments");
const likesCollection = db.collection("likes");

// export utils/refs
export {
  db,
  auth,
  usersCollection,
  postsCollection,
  commentsCollection,
  likesCollection,
};
