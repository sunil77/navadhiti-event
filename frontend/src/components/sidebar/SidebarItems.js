export default [
    // {
    //     icon: 'mdi-view-dashboard',
    //     title: 'Dashboard',
    //     to: '/dashboard',
    // },
    {
        title: "Events",
        icon: "mdi-calendar",
        router: "/events",
    },
    {
        title: "History",
        icon: "mdi-history",
        router: "/history",
    },
] 