import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login.vue";
// import firebase from "firebase";
// import { auth } from '../firebase'

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Login",
    component: Login,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/layout",
    name: "Layout",
    component: () => import("../components/layout/Layout.vue"),
    // meta: {
    //   authRequired: true,
    // },
    children: [
      {
        path: "/confirmation",
        name: "Confirmation",
        component: () => import("../views/confirmation/Confirmation"),
        props: true,
        // meta: {
        //   authRequired: true,
        // },
      },
      {
        path: "/events",
        name: "Events",
        component: () => import("../views/events/Events"),
        props: true,
        // meta: {
        //   authRequired: true,
        // },
      },
      {
        path: "/events/teams",
        name: "EventTeam",
        component: () => import("../views/events/EventTeam"),
        props: true,
        // meta: {
        //   authRequired: true,
        // },
      },
      {
        path: "/events/teammember",
        name: "Teams",
        component: () => import("../views/events/Teams"),
        // meta: {
        //   authRequired: true,
        // },
      },
      {
        path: "/events/generateteam",
        name: "GenerateTeam",
        component: () => import("../views/GenerateTeam.vue"),
        // meta: {
        //   authRequired: true,
        // },
      },
      {
        path: "/history",
        name: "History",
        component: () => import("../views/history/History.vue"),
        // meta: {
        //   authRequired: true,
        // },
      },
      {
        path: "/history/historyteam",
        name: "HistoryTeam",
        component: () => import("../views/history/HistoryTeam.vue"),
        // meta: {
        //   authRequired: true,
        // },
      },
      {
        path: "/history/teamrating",
        name: "TeamRating",
        component: () => import("../views/history/TeamRating.vue"),
        // meta: {
        //   authRequired: true,
        // },
      },
      {
        name: "Error",
        path: "*",
        component: () => import("@/views/Error.vue"),
      },
    ],
  },
];

let router = new VueRouter({
  mode: "history",
  routes,
});

// router.beforeEach((to, from, next) => {
//   if (to.matched.some((record) => record.meta.authRequired)) {
//     if (firebase.auth().currentUser) {
//       next();
//     } else {
//       // alert('You must be logged in to see this page');
//       next({
//         path: "/",
//       });
//     }
//   } else {
//     next();
//   }
// });

export default router;
